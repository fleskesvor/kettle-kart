extends CanvasLayer

func set_speed(percentage):
	$speed.value = percentage

func set_temperature(percentage):
	$temperature.value = percentage

func _ready():
	add_to_group("hud")