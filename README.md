Kettle Kart
========

The concept is a simple and silly racing game, where you have to manage the temperature of the racing kart, otherwise there will be consequences. When going faster, the kettle will heat up, and should produce more steam and obscure the track, which can be an advantage if you're in the lead. Going to fast for too long will make the lid pop though, and then you will have to wait to cool down before you can proceed. (Cue cutscene of tea being served.) There should be puddles along the track, which will slow down the kettle somewhat, but will also cool it down, so that you can keep momentum for longer.

NOTE! Because of real life obligations and the short time frame of the jam, few of my personal goals for the jam were met, and what's done is barely even a "tech demo". It is a concept I hope to flesh out little by little though, in the months following the jam.

Controls
--------

Increase speed - up arrow
Decrease speed - down arrow
Turn left - left arrow
Turn right - right arrow

Learning goals
-----------------

- Inkscape
- Autotiles
- Simple 2D physics
- Audio implementation
- Split screen multiplayer

TODO
-------

- Make "steam" depend on speed over time
- Implement more "realistic" accelleration+drift
- Add better art
- Lay tracks, puddles and decorations with autotiles
- Increase size of track
- Add second viewport for split screen multiplayer
- Add puddles with slow-down and cool-down
- Make particles look better
- Lots more
