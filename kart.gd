extends KinematicBody2D

# Speed variables in pixels per second
var speed = 0
const MAX_SPEED = 800

var temperature = 0
const MAX_TEMPERATURE = 200

func _physics_process(delta):

	var velocity = Vector2(0, -1)

	if Input.is_action_pressed("ui_up"):
		speed += 20
	elif Input.is_action_pressed("ui_down"):
		speed -= 20
	else:
		speed -= 5

	if Input.is_action_pressed("ui_left"):
		rotation_degrees -= 100 * delta
	if Input.is_action_pressed("ui_right"):
		rotation_degrees += 100 * delta

	if speed > MAX_SPEED:
		speed = MAX_SPEED
	elif speed < 0:
		speed = 0

	var angle = deg2rad(rotation_degrees)
	velocity = velocity.normalized() * speed
	velocity = velocity.rotated(angle)

	move_and_slide(velocity)

	# TODO: Should be longer cool-off on temperature than speed, and particle amount should depend on temperature
	$Particles2D.emitting = speed > 0
	get_tree().call_group("hud", "set_speed", float(speed) / MAX_SPEED * 100)

	temperature += 5 if speed > 600 else -5
	get_tree().call_group("hud", "set_temperature", float(temperature) / MAX_TEMPERATURE * 100)
